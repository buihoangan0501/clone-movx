import classNames from 'classnames/bind'
import styles from './upcoming.module.scss'
import img1 from '../../../../assets/phimavatar.jpg'

const cx = classNames.bind(styles)

function Upcoming() {
    return (
        <div className={cx('wrapper')}>
            <div className={cx('title')}>Upcoming Movies</div>
            <div className={cx('grid')}>
                <div className={cx('grid-item')}>
                    <div className={cx('grid-item-img')}>
                        <a href="/"><img src={img1} alt="" /></a>
                    </div>
                    <div className={cx('grid-item-details')}>The Super Mario Bros. Movie</div>
                    <div className={cx('grid-item-footer')}>
                        <p>2023</p>
                        <button><i className={cx('fa-solid fa-heart')}></i></button>
                    </div>
                </div>
                <div className={cx('grid-item')}>
                    <div className={cx('grid-item-img')}>
                        <a href="/"><img src={img1} alt="" /></a>
                    </div>
                    <div className={cx('grid-item-details')}>The Super Mario Bros. Movie</div>
                    <div className={cx('grid-item-footer')}>
                        <p>2023</p>
                        <button><i className={cx('fa-solid fa-heart')}></i></button>
                    </div>
                </div>
                <div className={cx('grid-item')}>
                    <div className={cx('grid-item-img')}>
                        <a href="/"><img src={img1} alt="" /></a>
                    </div>
                    <div className={cx('grid-item-details')}>The Super Mario Bros. Movie</div>
                    <div className={cx('grid-item-footer')}>
                        <p>2023</p>
                        <button><i className={cx('fa-solid fa-heart')}></i></button>
                    </div>
                </div>
                <div className={cx('grid-item')}>
                    <div className={cx('grid-item-img')}>
                        <a href="/"><img src={img1} alt="" /></a>
                    </div>
                    <div className={cx('grid-item-details')}>The Super Mario Bros. Movie</div>
                    <div className={cx('grid-item-footer')}>
                        <p>2023</p>
                        <button><i className={cx('fa-solid fa-heart')}></i></button>
                    </div>
                </div>
                <div className={cx('grid-item')}>
                    <div className={cx('grid-item-img')}>
                        <a href="/"><img src={img1} alt="" /></a>
                    </div>
                    <div className={cx('grid-item-details')}>The Super Mario Bros. Movie</div>
                    <div className={cx('grid-item-footer')}>
                        <p>2023</p>
                        <button><i className={cx('fa-solid fa-heart')}></i></button>
                    </div>
                </div>
                <div className={cx('grid-item')}>
                    <div className={cx('grid-item-img')}>
                        <a href="/"><img src={img1} alt="" /></a>
                    </div>
                    <div className={cx('grid-item-details')}>The Super Mario Bros. Movie</div>
                    <div className={cx('grid-item-footer')}>
                        <p>2023</p>
                        <button><i className={cx('fa-solid fa-heart')}></i></button>
                    </div>
                </div>
                <div className={cx('grid-item')}>
                    <div className={cx('grid-item-img')}>
                        <a href="/"><img src={img1} alt="" /></a>
                    </div>
                    <div className={cx('grid-item-details')}>The Super Mario Bros. Movie</div>
                    <div className={cx('grid-item-footer')}>
                        <p>2023</p>
                        <button><i className={cx('fa-solid fa-heart')}></i></button>
                    </div>
                </div>
                <div className={cx('grid-item')}>
                    <div className={cx('grid-item-img')}>
                        <a href="/"><img src={img1} alt="" /></a>
                    </div>
                    <div className={cx('grid-item-details')}>The Super Mario Bros. Movie</div>
                    <div className={cx('grid-item-footer')}>
                        <p>2023</p>
                        <button><i className={cx('fa-solid fa-heart')}></i></button>
                    </div>
                </div>
                <div className={cx('grid-item')}>
                    <div className={cx('grid-item-img')}>
                        <a href="/"><img src={img1} alt="" /></a>
                    </div>
                    <div className={cx('grid-item-details')}>The Super Mario Bros. Movie</div>
                    <div className={cx('grid-item-footer')}>
                        <p>2023</p>
                        <button><i className={cx('fa-solid fa-heart')}></i></button>
                    </div>
                </div>
                <div className={cx('grid-item')}>
                    <div className={cx('grid-item-img')}>
                        <a href="/"><img src={img1} alt="" /></a>
                    </div>
                    <div className={cx('grid-item-details')}>The Super Mario Bros. Movie</div>
                    <div className={cx('grid-item-footer')}>
                        <p>2023</p>
                        <button><i className={cx('fa-solid fa-heart')}></i></button>
                    </div>
                </div>
            </div>
            <button className={cx('buttons-view')}>View All Upcoming Movies</button>
        </div>
    )
}

export default Upcoming;