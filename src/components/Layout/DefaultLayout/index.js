import Header from "./Header/Header";
import Slider from "./Slider";
import Upcoming from "./Upcoming/upcoming";
import TopRated from "./TopRated/Toprated";
import classNames from 'classnames/bind'
import styles from './DefaultLayout.module.scss'

const cx = classNames.bind(styles)

function DefaultLayout({ children }) {
    return (
        <>
            <div className={cx('wrapper')}>
                <Header />
                <div className={cx('container')}>
                    <Slider />
                </div>
                <div className={cx('upcoming')}>
                    <Upcoming />
                </div>
                <div className={cx('toprated')}>
                    <TopRated />
                </div>
                <div className={cx('content')}>
                    {children}
                </div>
            </div>
        </>
    );
}

export default DefaultLayout;