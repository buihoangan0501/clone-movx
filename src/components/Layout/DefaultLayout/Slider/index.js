import classNames from 'classnames/bind'
import styles from './Slider.module.scss'
import img1 from '../../../../assets/phimavatar.jpg'
import img2 from '../../../../assets/PussInBoots.jpg'
import img3 from '../../../../assets/creed3.jpg'
import img4 from '../../../../assets/img5.jpg'
import img5 from '../../../../assets/wakanda.jpg'
import img6 from '../../../../assets/wakanda.jpg'

const cx = classNames.bind(styles)

function Slider() {

    const handleNext = () => {
        let lists = document.querySelectorAll('.slider-item');
        document.getElementsByClassName('.slider').appendChild(lists[0]);
    }

    return (
        <div className={cx('slider-container')}>
            <div className={cx('slider')}>
                <div className={cx('slider-item')}>
                    <img className={cx('slider-img')} src={img1} alt="" />
                    <div className={cx('overlay')}></div>
                    <div className={cx('slider-content')}>
                        <div className={cx('slider-content-name')}>AVATAR</div>
                        <div className={cx('slider-content-des')}>After dominating the boxing world, Adonis Creed has been thriving in both his career and family life.</div>
                        <button className={cx('slider-button')}>View Movie</button>
                    </div>
                </div>
                <div className={cx('slider-item')}>
                    <img className={cx('slider-img')} src={img1} alt="" />
                    <div className={cx('overlay')}></div>
                    <div className={cx('slider-content')}>
                        <div className={cx('slider-content-name')}>AVATAR</div>
                        <div className={cx('slider-content-des')}>After dominating the boxing world, Adonis Creed has been thriving in both his career and family life.</div>
                        <button className={cx('slider-button')}>View Movie</button>
                    </div>
                </div>
                <div className={cx('slider-item')}>
                    <img className={cx('slider-img')} src={img3} alt="" />
                    <div className={cx('slider-content')}>
                        <div className={cx('slider-content-name')}>AVATAR</div>
                        <div className={cx('slider-content-des')}>After dominating the boxing world, Adonis Creed has been thriving in both his career and family life.</div>
                        <button className={cx('slider-button')}>View Movie</button>
                    </div>
                </div>
                <div className={cx('slider-item')}>
                    <img className={cx('slider-img')} src={img4} alt="" />
                    <div className={cx('slider-content')}>
                        <div className={cx('slider-content-name')}>AVATAR</div>
                        <div className={cx('slider-content-des')}>After dominating the boxing world, Adonis Creed has been thriving in both his career and family life.</div>
                        <button className={cx('slider-button')}>View Movie</button>
                    </div>
                </div>
                <div className={cx('slider-item')}>
                    <img className={cx('slider-img')} src={img5} alt="" />
                    <div className={cx('slider-content')}>
                        <div className={cx('slider-content-name')}>AVATAR</div>
                        <div className={cx('slider-content-des')}>After dominating the boxing world, Adonis Creed has been thriving in both his career and family life.</div>
                        <button className={cx('slider-button')}>View Movie</button>
                    </div>
                </div>
                <div className={cx('slider-item')}>
                    <img className={cx('slider-img')} src={img6} alt="" />
                    <div className={cx('slider-content')}>
                        <div className={cx('slider-content-name')}>AVATAR</div>
                        <div className={cx('slider-content-des')}>After dominating the boxing world, Adonis Creed has been thriving in both his career and family life.</div>
                        <button className={cx('slider-button')}>View Movie</button>
                    </div>
                </div>
            </div>
            <div className={cx('buttons')}>
                <button className={cx('prev', 'buttons-prev')}><i className={cx('fa-solid fa-angle-left', 'buttons-i')}></i></button>
                <button onClick={handleNext} className={cx('next', 'buttons-prev')}><i className={cx('fa-solid fa-angle-right', 'buttons-i')}></i></button>
            </div>
        </div>
    )
}

export default Slider;