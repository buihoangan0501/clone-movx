import classNames from 'classnames/bind'
import styles from './Toprated.module.scss'
import img1 from '../../../../assets/phimavatar.jpg'

const cx = classNames.bind(styles)

function TopRated() {
    return (
        <div className={cx('wrapper')}>
            <div className={cx('title')}>TopRated Movies</div>
            <div className={cx('grid')}>
                <div className={cx('grid-item')}>
                    <div className={cx('grid-item-img')}>
                        <a href="/"><img className={cx('grid-item-img-i')} src={img1} alt="" /></a>
                    </div>
                    <div className={cx('grid-item-details')}>The Super Mario Bros. Movie</div>
                    <div className={cx('grid-item-footer')}>
                        <p>2023</p>
                        <button><i className={cx('fa-solid fa-heart')}></i></button>
                    </div>
                </div>
            </div>
            <button className={cx('buttons-view')}>View All TopRated Movies</button>
        </div>
    )
}

export default TopRated;