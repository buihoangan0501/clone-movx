import classNames from 'classnames/bind'
import styles from './Header.module.scss'
import { useState, useEffect } from "react";

const cx = classNames.bind(styles)

function Header() {

    const [navbar, setNavbar] = useState(false)

    useEffect(() => {
        const changeBackground = () => {
            if (window.scrollY >= 100) {
                setNavbar(true)
            } else {
                setNavbar(false)
            }
        }
        window.addEventListener('scroll', changeBackground)

        return () => {
            window.removeEventListener('scroll', changeBackground)
        }
    }, []);

    return (
        <>
            <div className={cx(navbar ? 'active' : 'navigation')}>
                <div className={cx('navigation-wrapper')}>
                    <div className={cx('navigation-logo')}></div>
                    <div className={cx('navigation-menu-wrapper')}>
                        <div className={cx('navigation-menu')}>
                            <ul className={cx('navigation-list')}>
                                <li className={cx('navigation-item')}>
                                    <a href="/" className={cx('navigation-active')}>Home</a>
                                </li>
                                <li className={cx('navigation-item')}>
                                    <a className={cx('navigation-link')} href="/">Trending</a>
                                </li>
                                <div className={cx('navigation-dropdown', 'navigation-item')}>
                                    <a className={cx('navigation-link')} href="/">Discover</a>
                                    <div className={cx('navigation-dropdown-wrapper')}>
                                        <a className={cx('navigation-link-dropdown')} href="/">Popular</a>
                                        <a className={cx('navigation-link-dropdown')} href="/">Upcoming</a>
                                        <a className={cx('navigation-link-dropdown')} href="/">Top Rated</a>
                                    </div>
                                </div>
                                <li className={cx('navigation-item')}>
                                    <a className={cx('navigation-link')} href="/">TV Shows</a>
                                </li>
                                <li className={cx('navigation-item')}>
                                    <a className={cx('navigation-link')} href="/">People</a>
                                </li>
                                <li className={cx('navigation-item')}>
                                    <a className={cx('navigation-link')} href="/">Genres</a>
                                </li>
                                <li className={cx('navigation-item')}>
                                    <a className={cx('navigation-link')} href="/">Favorites</a>
                                </li>
                            </ul>
                        </div>
                        <div className={cx('navigation-search')}>
                            <input className={cx('navigation-input')} type="text" placeholder='Search' />
                            <i className={cx('fa-solid fa-magnifying-glass', 'navigation-i')}></i>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Header;